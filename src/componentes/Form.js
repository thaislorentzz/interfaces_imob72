import React from 'react';
import useForms from './useForms'
import MaskedInput from 'react-text-mask'

const Form = () => {

    const formulario = () => {
        console.log(inputs);
    };

    const [inputs, setInputs, handleSubmit] = useForms(formulario);

    return (
        <form onSubmit={handleSubmit}>
      <fieldset>
        <legend>Cadastro Empresa</legend>

        <label htmlFor="nome">Nome</label>
        <input
          type="text"
          placeholder="Nome"
          id="nome"
          name="nome"
          onChange={setInputs}
          value={inputs.nome}></input>

        <label htmlFor="telefone">Telefone</label>
        <MaskedInput
              type="text"
              placeholder="(00) 00000-0000"
              maxLength="17"
              mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/,/\d/, '-', /\d/, /\d/, /\d/, /\d/]}
              guide={true}
              id="telefone"
              name="telefone"
              onChange={setInputs}
              value={inputs.telefone} />

        <label htmlFor="cnpj">CNPJ</label>
        <MaskedInput
            type="text"
            placeholder="00.000.000/0000-00"
            maxLength="18"
            mask={[/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]}
            guide={false}
            id="cnpj"
            name="cnpj"
            onChange={setInputs}
            value={inputs.cnpj}/>

        <label htmlFor="cep">CEP</label>
        <MaskedInput
            className="input-botao"
            type="text"
            placeholder="00000-000"
            maxLength="15"
            mask={[/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
            guide={false}
            id="cep"
            name="cep"
            onChange={setInputs}
            value={inputs.cep}/>
        <input 
          type="button"></input>

        <label htmlFor="rua">Rua</label>
        <input
          type="text"
          placeholder="Rua"
          id="rua"
          name="rua"
          onChange={setInputs}
          value={inputs.rua}></input>

        <label htmlFor="bairro">Bairro</label>
        <input
          type="text"
          placeholder="Bairro"
          id="bairro"
          name="bairro"
          onChange={setInputs}
          value={inputs.bairro}></input>

        <label htmlFor="cidade">Cidade</label>
        <input
          type="text"
          placeholder="Cidade"
          id="cidade"
          name="cidade"
          onChange={setInputs}
          value={inputs.cidade}></input>

        <label htmlFor="estado">Estado</label>
        <input
          type="text"
          placeholder="Estado"
          id="estado"
          name="estado"
          onChange={setInputs}
          value={inputs.estado}></input>

        <label htmlFor="complemento">Complemento</label>
        <input
          type="text"
          placeholder="Complemento"
          id="complemento"
          name="complemento"
          onChange={setInputs}
          value={inputs.complemento}></input>

        <label htmlFor="numero">Número</label>
        <input
          type="text"
          placeholder="Número"
          id="numero"
          name="numero"
          onChange={setInputs}
          value={inputs.numero}></input>

        <button type="submit" >Enviar</button>

    </fieldset >
    </form >  
    );
}

export default Form;